# iam-example
> An example for in-app purchases

## Notes

- Do not directly log `ctx` to the console or the application will freeze. (e.g. `console.log(ctx)`).
